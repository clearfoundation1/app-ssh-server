<?php

$lang['ssh_server_allow_passwords'] = 'Дозволити вхід за паролем';
$lang['ssh_server_allow_root_login'] = 'Дозволити вхід для root';
$lang['ssh_server_allow_tcp_forwarding'] = 'Дозволити forwarding TCP';
$lang['ssh_server_app_description'] = 'Додаток SSH-сервер надає інструменти для керування політиками безпечної оболонки для вашої системи.';
$lang['ssh_server_app_name'] = 'SSH-сервер';
$lang['ssh_server_app_tooltip'] = 'Змінення налаштувань безпеки сервера SSH може використовуватися для досягнення відповідності нормативним вимогам (наприклад PCI) або застосування політики компанії щодо стандартів безпеки.';
$lang['ssh_server_password_authentication_policy_invalid'] = 'Політика автентифікації за паролем недійсна.';
$lang['ssh_server_permit_root_login_policy_invalid'] = 'Політика дозволу входу для користувача root недійсна.';
$lang['ssh_server_ssh_bruteforce_detection'] = 'SSH Brute Force Detection';
$lang['ssh_server_ssh_ddos_detection'] = 'SSH DDoS Detection';
